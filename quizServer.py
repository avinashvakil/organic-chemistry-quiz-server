# Copyright Avinash Vakil 2013

#from flask.ext.wtf import *

from bottle import *

import os, sys

import shutil

import time

import random

import thread

import cPickle as pickle

import datetime



import cherrypy

from cherrypy import wsgiserver

from cherrypy.process.servers import ServerAdapter

#def convertChemical(informat,outformat,inputMolecule,gen2d=False):
#	import pybel
#	assert informat in pybel.informats.keys(),"BAD INPUT TYPEKEY"
#	assert outformat in pybel.outformats.keys(),"BAD OUTPUT TYPEKEY"
#	MOL=inputMolecule
#	mymol = pybel.readstring(informat, MOL)
#	mymol.make3D()
#	print mymol.molwt
#	return mymol.write(outformat) 
Chemo={}
def convertChemical(informat,outformat,inputMolecule,gen2d=False):
	while len(Chemo.keys())>100:
		print "Pruning the molecule cache..."
		del Chemo[random.choice(Chemo.keys())]
	if inputMolecule+informat+outformat not in Chemo.keys():
		print "don't know this molecule..."
		import openbabel
		obConversion = openbabel.OBConversion()
		obConversion.SetInAndOutFormats(informat,outformat)
		
		mol = openbabel.OBMol()	
		obConversion.ReadString(mol, inputMolecule)
		if gen2d:
			gen2d = openbabel.OBOp.FindType("gen2d") # Looks up the plugin and returns it
			gen2d.Do(mol) # Calls the plugin on your molecule
		
		Chemo[inputMolecule+informat+outformat]=obConversion.WriteString(mol)
		
		return Chemo[inputMolecule+informat+outformat]
	else:
		print "already know molecule..."
		return Chemo[inputMolecule+informat+outformat]

	
def literalNewLine(inputText):
	return inputText.replace("\n","\\n")


class aUser:
	def __init__(self,username,storageDirectory,password):
		print "writing new user", username
		self.username=username
		self.password=password
		self.storageDirectory=storageDirectory
		self.reactionResults={}
		self.reactionResultFractions={}
		self.writeSelf()

	def deleteSelf(self):
		userfileObject=os.remove(os.path.join(self.storageDirectory,self.username))
		print "deleted userdata for",self.username

	def writeSelf(self):
		userfileObject=open(os.path.join(self.storageDirectory,self.username),"w")
		loadedUser=pickle.dump(self,userfileObject)
		userfileObject.close()
		
	def initializeReactions(self):
		self.reactions=reactions
		
	def saveResult(self,GUID,result):
		self.reactionResults[GUID]=self.reactionResults.get(GUID,[])+[result]

		accumulator=0
		resultsForPercentage=self.reactionResults[GUID]
		for scorevalue in resultsForPercentage:
			accumulator+=int(scorevalue)
		
		self.reactionResultFractions[GUID]=float(accumulator)/len(resultsForPercentage)
		print self.reactionResults,self.reactionResultFractions
		self.writeSelf()
		
	def cleanup(self,reactionsAvailable,storageDir):
		if storageDir != self.storageDirectory:
			print "storage directory moved for user",self.username
			self.storageDirectory=storageDir
		print "checking if user", self.username, "needs cleaning"
		validreactionGUIDs=[]
		for reaction in reactionsAvailable:
			validreactionGUIDs.append(reaction[0].GUID)
		for loadedRxn in self.reactionResults.copy():
			if loadedRxn not in validreactionGUIDs:
				del(self.reactionResults[loadedRxn])
				print "deleted",loadedRxn,"result data from",self.username
		for loadedRxn in self.reactionResultFractions.copy():
			if loadedRxn not in validreactionGUIDs:
				del(self.reactionResultFractions[loadedRxn])
				print "deleted",loadedRxn,"result fraction data from",self.username
		self.writeSelf()




class userManager:
	def __init__(self,userDirectory,questionManager):
		self.userDirectory=userDirectory
		self.users=[]
		self.usernames={}
		userFiles=os.listdir(self.userDirectory)
		userFiles  = filter(lambda fname: not fname.startswith("."), userFiles)

		for userfile in userFiles:
			userfileObject=open(os.path.join(userDirectory,userfile),"r")
			loadedUser=pickle.load(userfileObject)
			userfileObject.close()
			self.users.append(loadedUser)
			self.usernames[loadedUser.username]=loadedUser
			print "unpickled user", loadedUser.username,"|",
			loadedUser.cleanup(questionManager.reactions,self.userDirectory)
		
	def newUser(self,username,password):
		newUser=aUser(username,self.userDirectory,password)
		self.users.append(newUser)
		self.usernames[username]=newUser
		
	def removeUser(self,username):
		self.usernames[username].deleteSelf()
		self.users.remove(self.usernames[username])
		del(self.usernames[username])
		print username,"has been removed from the users list"
			

class reactionItem:
	def __init__(self,reactionName,questionsDirectory,variantfile,GUID,infoText):
		print "new reaction",reactionName
		self.reactionName=reactionName
		self.reactionSpecificationFile=os.path.join(questionsDirectory,reactionName,variantfile)
		self.reactionInfo={}
		self.hidableInfo=[]
		self.variant=variantfile.strip(".txt")
		self.GUID=GUID
		self.infoText=infoText
		try:
			self.reactionIndex=float(self.reactionName.split(" ")[0])
		except:
			self.reactionIndex=1000.0
			print "Warning:",self.reactionName,"does not have a sorting index. Using 1000."

		rxninfofile=open(self.reactionSpecificationFile,"r+")
		RXNinfotext=rxninfofile.readlines()
		print self.reactionSpecificationFile
		rxninfofile.close()
		RXNinfotext=map(lambda line:line.strip().split("$$$"),RXNinfotext)
		RXNinfotextDICT={}
		for infoItem in RXNinfotext:
			if "reactant" in infoItem[0] or "product" in infoItem[0]:
				RXNinfotextDICT[infoItem[0]]=convertChemical("smi","sdf", infoItem[1], gen2d=True)
			else:
				RXNinfotextDICT[infoItem[0]]=infoItem[1]
		self.reactionInfo=RXNinfotextDICT
		#print self.reactionInfo
		
		hidableTemp=[]
		for infoItem in self.reactionInfo.keys():
			if infoItem == "conditions":
				pass
			else:
				#hidableTemp.append(infoItem) #disabled asking anything excepting products!
				if "product" in infoItem:
					hidableTemp.append(infoItem)
					hidableTemp.append(infoItem)
					hidableTemp.append(infoItem)
		
		
		self.hidableInfo=hidableTemp
		print self.hidableInfo

#		for possibleProperty in [["reactant 2",1],["reactant 1",1],["solvent",1],["conditions",0],["legends",0],["major organic product",3],["non organic product",1]]:
#			testPath=os.path.join(self.reactionSpecificationFile,possibleProperty[0]+".png")
#			if os.path.isfile(testPath):
#				self.reactionInfo[possibleProperty[0]]=[testPath]
#				for doubledip in xrange(possibleProperty[1]):
#					self.hidableInfo.append(possibleProperty[0])
#				altindex=1
#				while True:
#					testPathAlts=os.path.join(self.reactionSpecificationFile,possibleProperty[0]+"ALT"+str(altindex)+".png")
#					if os.path.isfile(testPathAlts):
#						self.reactionInfo[possibleProperty[0]].append(testPathAlts)
#						altindex+=1
#					else:
#						break
		
		#print self.reactionInfo
		print self.hidableInfo
		
	def updateUserResults(self,someUser,theResult):
		someUser.saveResult(self.GUID,theResult)


class questionItem:
	def __init__(self,aReaction,baseDirectory,missingItem=None):
		self.reaction=aReaction
		reactionCopy=self.reaction.hidableInfo[:]
		random.shuffle(reactionCopy)
		self.missingInfoName=self.reaction.hidableInfo[:]		
		self.missingInfoItem=map(lambda x: literalNewLine(self.reaction.reactionInfo[x]), self.missingInfoName)
		self.missingInfoItemINCHIKEY=map(lambda x:literalNewLine(convertChemical("sdf","inchikey",self.reaction.reactionInfo[x])),self.missingInfoName)

		self.InfoItems={}
		for reactionInfo in self.reaction.reactionInfo.keys():			self.InfoItems[reactionInfo]=literalNewLine(self.reaction.reactionInfo[reactionInfo])
		
	def __str__(self):
		newStr="Given that:\n"
		for infoItem in self.InfoItems.keys():
			newStr+=infoItem+ " is "+ self.InfoItems[infoItem]+"\n"
		newStr+= "What is "+ self.missingInfoName+"?\n"
		newStr+= "Answer:"+self.missingInfoItem
		return newStr

		


class questionManager:
	def __init__(self,questionsDirectory):
		print "made"
		self.reactions=[]
		self.reactionNames={}
		self.reactionGUIDs={}
		self.reactionNamesVariantCounts={}
		self.baseDirectory=questionsDirectory
		rawReactionList = []
		for diritem in os.walk(questionsDirectory,topdown=True):
			if str(questionsDirectory) == str(diritem[0]):
				for dir in diritem[1]:
					rawReactionList.append(dir)
		print rawReactionList
		for reaction in rawReactionList:
			variantList=[]
			RXNGUID=None
			for diritem in os.walk(questionsDirectory+"/"+reaction,topdown=True):
				if str(questionsDirectory+"/"+reaction) == str(diritem[0]):
#					for dir in diritem[1]:
#						variantList.append(dir)
					for rxnfile in diritem[2]:
						if "GUID" in rxnfile:
							RXNGUID=rxnfile
							rxninfofile=open(os.path.join(questionsDirectory,reaction,RXNGUID),"r+")
							RXNinfotext=rxninfofile.readlines()
							rxninfofile.close()
						elif rxnfile[0]!=".":
							variantList.append(rxnfile)
#						if "INFORMATION" in GUIDCandidate:
#							RXNINFORMATION=GUIDCandidate
#							rxnmoldatafile=open(os.path.join(questionsDirectory,reaction,RXNINFORMATION),"r+")
#							rxnmoldata=rxnmoldatafile.readlines()
#							rxnmoldatafile.close()
			variantRXNItems=[]
			for variant in variantList:
				newReactionItem=reactionItem(reaction,questionsDirectory,variant,RXNGUID,RXNinfotext)
				variantRXNItems+=[newReactionItem]
			self.reactions.append(variantRXNItems)
			self.reactionNames[reaction]=variantRXNItems
			self.reactionGUIDs[RXNGUID]=variantRXNItems
			self.reactionNamesVariantCounts[reaction]=len(self.reactionNames[reaction])
		print self.reactions
		
	def generateQuestion(self,weightedForUser=None):
		if weightedForUser!=None:
			print "creating weighted question"
			reactionToReturn=None
			reactionCopy=self.reactions[:]
			random.shuffle(reactionCopy)
			#print reactionCopy
			tryindex=0
			while reactionToReturn==None:
				randomNumber=random.random()
				randomReaction=reactionCopy[tryindex]
				scoreRawData=weightedForUser.reactionResultFractions.get(randomReaction[0].GUID,0.0)
				print scoreRawData
				fractionCorrect=scoreRawData
				#print "data for", randomReaction.reactionName, fractionCorrect, "vs random num", randomNumber
				if randomNumber > fractionCorrect*0.75:
					reactionToReturn=random.choice(randomReaction)
					randomReaction=reactionToReturn
				else:
					#print "skipping", randomReaction.reactionName
					tryindex+=1
					if tryindex==len(reactionCopy):
						tryindex=0
			
		else:
			reactionCopy=self.reactions[:]
			random.shuffle(reactionCopy)
			randomReaction=random.choice(reactionCopy[0])
		return questionItem(randomReaction,self.baseDirectory)
		
	def returnGivenQuestion(self,reaction,variant=None):
		reactionList=self.reactionGUIDs.get(reaction,None)
		print variant
		if reactionList!=None:
			if variant==None:
				reaction=random.choice(reactionList)
				return questionItem(reaction,self.baseDirectory)
			else:
				for reaction in reactionList:
					if reaction.variant==variant:
						return questionItem(reaction,self.baseDirectory)
				return None
		else:
			return None





#@post('/revert/<section>/<item>')

#def revertENB(section,item):

#	#Gets the text box value and the url pattern to call the object's setter method.

#	print list(ENB.getbackup(section,item))

#	ENB.placeNewValue(section,item,list(ENB.getbackup(section,item))[0])

#	return redirect("/" + "#" + section + "X" + item)

class quizApp(Bottle):
	def __init__(self,reactionsPath,userPath,secretSignup,secretAdmin):
		Bottle.__init__(self)
		self.questionManagerInstance=questionManager(reactionsPath)
		self.userManagerInstance=userManager(userPath,self.questionManagerInstance)
		self.rxnPath=reactionsPath
		self.userPath=userPath
		self.secretSignup=secretSignup
		self.secretAdmin=secretAdmin
		print "QUIZ IS READY"
	

		def requiresAuthentication(original_function):
			# make a new function that prints a message when original_function starts and finishes
			def new_function(*args, **kwargs):
				username=kwargs["username"]
				if username!="guest":
					loginSessionTimeout=600
					print "Authenticated Page Requested", request.path
					if not request.get_cookie("account", secret=self.secretAdmin) or not request.get_cookie("settime", secret=self.secretAdmin):
						redirect("/")
					cookieSetTime=request.get_cookie("settime", secret=self.secretAdmin)
					cookieTimeDelta=datetime.datetime.now()-cookieSetTime
					if loginSessionTimeout<cookieTimeDelta.total_seconds() or request.get_cookie("account", secret=self.secretAdmin)!=username:
						redirect("/")
					print "Session Active For:",cookieTimeDelta
					response.set_cookie("settime",datetime.datetime.now(),path="/", secret=self.secretAdmin)
				return original_function(*args, **kwargs)
			return new_function

		@self.route('/login',method='POST')
		def loginUser():
			username=str(request.forms.username)
			password=str(request.forms.password)
			print username,password
			if username in self.userManagerInstance.usernames.keys() and self.userManagerInstance.usernames[username].password==password:
				print username, "logged in"
				response.set_cookie("account", username,path="/", secret=self.secretAdmin)
				response.set_cookie("settime",datetime.datetime.now(),path="/", secret=self.secretAdmin)
				return redirect("/main/"+username+"/menu")
			else:
				print "invalid user"
				
		@self.route('/logout',method='GET')
		def logout():
			response.delete_cookie("account")
			response.delete_cookie("settime")
			return redirect("/")


	

		@self.route('/static/<filename:path>')

		def server_static(filename):

			return static_file(filename, root='static')


		@self.route('/reactionResource/quizItems/<filename:path>')
		def server_static(filename):
			return static_file(filename, root='quizItems')
	

	

		@self.route('/')

		def sendToTiles():

			return template("loginWindow.html")


		@self.route('/main/<username>/menu')
		@requiresAuthentication
		def showMenu(username):
			if username!="guest":
				try:
					theUser=self.userManagerInstance.usernames[username]
				except:
					return "invalid username"
				return template("quizMenuView.html",theUser=theUser,reactions=self.questionManagerInstance)
			else:
				return template("quizMenuView.html",theUser="GUEST",reactions=self.questionManagerInstance)

		@self.route('/showReaction/<username>/<reaction>/<variant>')
		@requiresAuthentication
		def showquestion(username,reaction,variant):
			try:
				reaction=self.questionManagerInstance.returnGivenQuestion(reaction,variant=variant)
				assert reaction!=None
			except:
				return "invalid reaction requested"
			if username!="guest":
				try:
					theUser=self.userManagerInstance.usernames[username]
				except:
					return "invalid username"
			else:
				theUser="GUEST"
			return template("quizQuestionView.html",theuser=theUser,quizItem=reaction,mode="study")



		@self.route('/main/<username>/react')
		@requiresAuthentication
		def showgradedquestion(username):
			if username!="guest":
				try:
					theUser=self.userManagerInstance.usernames[username]
				except:
					return "invalid username"
				return template("quizQuestionView.html",theuser=theUser,quizItem=self.questionManagerInstance.generateQuestion(weightedForUser=theUser),mode="quiz")
			else:
				return template("quizQuestionView.html",theuser="GUEST",quizItem=self.questionManagerInstance.generateQuestion(),mode="quiz")


		@self.route('/main/<username>/logresult/<GUID>/<result>')
		@requiresAuthentication
		def logresult(username,GUID,result):
			sentBackTo=request.headers.get('Referer',None)
			try:
				theUser=self.userManagerInstance.usernames[username]
				theReaction=self.questionManagerInstance.reactionGUIDs[GUID]
			except:
				return "invalid username or reaction name"
			if result=="1":
				result=True
			elif result=="0":
				result=False
			theUser.saveResult(GUID,result)
			print "saved that",username,"got",result,"on",GUID
			if sentBackTo!=None:
				return redirect(sentBackTo)
			else:
				return redirect("/")

		@self.route('/newUser/<username>/<password>/<adminPass>')
		def newUser(username,password,adminPass):
			if adminPass==self.secretAdmin:
				try:
					self.userManagerInstance.newUser(username,password)
					return "user created with name ",username
				except:
					return "failed to create user"
			else:
				print "wrong password"
			
		@self.route('/deleteUser/<username>/<adminPass>')
		def delUser(username,adminPass):
			if adminPass==self.secretAdmin:
				try:
					self.userManagerInstance.removeUser(username)
					return "user deleted with name ",username
				except:
					return "failed to delete user"
			else:
				print "wrong password"

		@self.route('/reloadServer')
		def reloadserver():
			self.questionManagerInstance=questionManager(self.rxnPath)
			self.userManagerInstance=userManager(self.userPath,self.questionManagerInstance)
			print "QUIZ IS READY"



		@self.route('/register')
		def showregister():
			return template("registerAccount.html")

		@self.route('/convertCE/<informat>/<outformat>',method="POST")
		def getKey(informat,outformat):
			MOL=str(request.forms.molfile)

			return convertChemical(informat,outformat,MOL).replace("\n","")

		@self.route('/createAccount',method="POST")
		def createAccountClient():
			username=str(request.forms.username)
			password=str(request.forms.password)
			secret=str(request.forms.sharedsecret)
			if secret==self.secretSignup:
				print username,password,secret
				if username not in self.userManagerInstance.usernames.keys():
					try:
						self.userManagerInstance.newUser(username,password)
						return "user created with name ",username
					except:
						return "failed to create user"
				else:
					return "This username already exists. Please choose a new one."
			else:
				return "Shared Secret Incorrect!"
			return "DONE"


if __name__ == "__main__":
	
	import argparse

	parser = argparse.ArgumentParser(description='Process some integers.')
	parser.add_argument('-r','--reactions',default="quizItems")
	parser.add_argument('-u','--users',default="users")
	parser.add_argument('-p','--port',default="1330")
	parser.add_argument('-s','--secretSignUp',default="OCHEM4WHITTIER")
	parser.add_argument('-a','--secretAdmin',default="4g8fdgdg4df869g48fg8964986")

	args = parser.parse_args()
	globalRXNDIR=args.reactions
	globalUSRDIR=args.users
	secretSignup=args.secretSignUp
	secretAdmin=args.secretAdmin

	
	
	app=quizApp(globalRXNDIR,globalUSRDIR,secretSignup,secretAdmin)

	serverplaintext = wsgiserver.CherryPyWSGIServer(('0.0.0.0', int(args.port)), app,server_name='10.0.0.67')



	s2=ServerAdapter(cherrypy.engine,serverplaintext)

	s2.subscribe()

	cherrypy.engine.start()

	cherrypy.engine.block()



