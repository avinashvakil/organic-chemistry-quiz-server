# Organic Chemistry Quiz Server
This is a Python based server that delivers organic chemistry questions to users, drawn from an easily created pool of questions. Not only does it track user's proficiency and tailor the questions asked to their performance, users must draw in the molecules for automatic answer checking (which keeps users from blowing off questions they *think* they already know).

## Dependencies
This Python web application requires:

* Python 2.7
* Cherrypy (as web server)
* openbabel (for molecule parsing)

## Thanks
The *Organic Chemistry Quiz Server* would not be possible without

* [bottle](http://bottlepy.org) (excellent and self contained wsgi web framework)
* [chemdoodle web frameworks](http://web.chemdoodle.com) (javascript/html5 molecule sketcher)