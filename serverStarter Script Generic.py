from quizServer import *
rxnDAY=quizApp("reactionDayVersion/reactions","reactionDayVersion/users","account creation pass","admin pass")
rxnALL=quizApp("allReactionsVersion/reactions","allReactionsVersion/users","account creation pass","admin pass")
serverrxnDAY = wsgiserver.CherryPyWSGIServer(('0.0.0.0', 1330), rxnDAY,server_name='10.0.0.67')
serverrxnALL = wsgiserver.CherryPyWSGIServer(('0.0.0.0', 1331), rxnALL,server_name='10.0.0.67')

s1=ServerAdapter(cherrypy.engine,serverrxnALL)
s1.subscribe()
s2=ServerAdapter(cherrypy.engine,serverrxnDAY)
s2.subscribe()
cherrypy.engine.start()
cherrypy.engine.block()
